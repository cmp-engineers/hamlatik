<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Admin\Api'], function (){

   Route::get('vendors','ApiController@vendors');

   Route::get('vendor/{id}','ApiController@vendor');

   Route::get('advertises','ApiController@advertises');

   Route::get('advertise/{id}', 'ApiController@advertise');

   Route::get('categories','ApiController@categories');

   Route::get('category/{id}','ApiController@category');

   Route::get('cities','ApiController@cities');

   Route::get('countries', 'ApiController@countries');

   Route::get('country/{id}', 'ApiController@country');

   Route::get('citiesByCountry/{country_id}', 'ApiController@citiesByCountry');

   Route::get('city/{id}','ApiController@city');

   Route::get('events','ApiController@events');

   Route::get('event/{id}', 'ApiController@event');

   Route::get('offers','ApiController@offers');

   Route::post('offerByVendor', 'ApiController@offerByVendor');

   Route::get('offer/{id}', 'ApiController@offer');

   Route::get('places', 'ApiController@places');

   Route::get('place/{id}','ApiController@place');

//   Route::post('placeByCategoryCity', 'ApiController@placeByCategoryCity');

    Route::get('placeByCountryCityCategory/{country_id}/{city_id}/{category_id}', 'ApiController@placeByCountryCityCategory');
   // Route::get('placeByCategoryCountryCity', 'ApiController@placeByCategoryCountryCity');

   Route::get('prayerByNameCategorySubCategory/{id1}/{id2}/{id3}', 'ApiController@prayerByNameCategorySubCategory');

   Route::get('prayerCategories', 'ApiController@prayerCategories');

   Route::post('prayerCategory', 'ApiController@prayerCategory');

   Route::get('prayerCategoryByName', 'ApiController@prayerCategoryByName');

   Route::get('prayerSubCategories', 'ApiController@prayerSubCategories');

   Route::get('prayerSubCategory/{id}', 'ApiController@prayerSubCategory');

   Route::get('PrayerCategoryPrayerSubCategory/{id1}/{id2}', 'ApiController@PrayerCategoryPrayerSubCategory');

   Route::get('prayerNames', 'ApiController@prayerNames');

   Route::get('prayerName/{id}', 'ApiController@prayerName');

   Route::get('prayerNamesCategoryPrayers', 'ApiController@prayerNamesCategoryPrayers');


});
