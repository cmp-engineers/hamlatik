<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// add middleware => 'auth:admin'
Route::group(['namespace' => 'Admin'], function() {

    Route::get('admin/home', 'HomeController@index')->name('admin.home');

    // Advertise
    Route::resource('admin/advertise', 'AdvertiseController');

    // City
    Route::resource('admin/city', 'CityController');

    // Event
    Route::resource('admin/event', 'EventController');

    // Offer
    Route::resource('admin/offer', 'OfferController');

    // Place
    Route::resource('admin/place', 'PlaceController');

    // Prayer 
    Route::resource('admin/prayer', 'PrayerController');

    // Vendor
    Route::resource('admin/vendor', 'VendorController');

    // Category for cities
    Route::resource('admin/category','CategoryController');

    // Prayer Category
    Route::resource('admin/prayercategory', 'PrayerCategoryController');

    // Prayer sub Category
    Route::resource('admin/prayersubcategory','PrayerSubCategoryController');

    // Prayer Names
    Route::resource('admin/prayername','PrayerNameController');

    // Country
    Route::resource('admin/country', 'CountryController');

    // Permissions
    Route::resource('admin/permission','PermissionController');

    // Role
    Route::resource('admin/role', 'RoleController');

    // Admin user
    Route::resource('admin/user', 'UserController');

    // Admin Profile
    Route::get('admin/profile/{admin}','ProfileController@edit')->name('admin.profile');
    Route::patch('admin/profile-update/{admin}','ProfileController@update')->name('admin.profile-update');

    // Admin Auth routes
    Route::get('admin-login','Auth\LoginController@showLoginForm')->name('admin.login');

    // the name admin-login must be the same
    Route::post('admin-login', 'Auth\LoginController@login');



});


Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
