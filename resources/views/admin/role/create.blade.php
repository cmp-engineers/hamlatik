@extends('admin.layouts.app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Titles</h3>
                        </div>


                    @include('includes.messages')


                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" action="{{ route('role.store') }}">
                            <div class="box-body">

                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-offset-3 col-lg-6">

                                        <div class="form-group">
                                            <label for="tag">Role Title</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Tag title">
                                        </div>

                                        <div class="col-lg-3">
                                            <label for="name">super admin Permission</label>
                                            @foreach($permissions as $permission)
                                                @if($permission->for == 'super-admin')
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}</label>

                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>

                                        <div class="col-lg-3">
                                            <label for="name">vendor Permission</label>
                                            @foreach($permissions as $permission)
                                                @if($permission->for == 'vendor')
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}</label>

                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>

{{--                                        <div class="col-lg-3">--}}
{{--                                            <label for="name">Posts Permission</label>--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                @if($permission->for == 'admin')--}}
{{--                                                    <div class="checkbox">--}}
{{--                                                        <label><input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}</label>--}}

{{--                                                    </div>--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}

{{--                                        <div class="col-lg-3">--}}
{{--                                            <label for="name">user Permission</label>--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                @if($permission->for == 'role')--}}
{{--                                                    <div class="checkbox">--}}
{{--                                                        <label><input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}</label>--}}

{{--                                                    </div>--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}

{{--                                        <div class="col-lg-3">--}}
{{--                                            <label for="name">Other Permission</label>--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                @if($permission->for == 'permission')--}}
{{--                                                    <div class="checkbox">--}}
{{--                                                        <label><input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}</label>--}}

{{--                                                    </div>--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}

{{--                                        <div class="col-lg-3">--}}
{{--                                            <label for="name">admin Permission</label>--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                @if($permission->for == 'advertise')--}}
{{--                                                    <div class="checkbox">--}}
{{--                                                        <label><input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}</label>--}}

{{--                                                    </div>--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a type="button" class="btn btn-warning" href="{{ route('role.index') }}">Back</a>
                                        </div>
                                    </div> <!--  -->


                                </div> <!-- end row -->


                            </div>
                            <!-- /.box-body -->

                        </form>

                    </div>

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection