@extends('admin.layouts.app')


@section('main-content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Hamlat
            <small>Hamlat</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Editors</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Titles</h3>
                </div>
                @include('includes.messages')
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('place.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="title">Place Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                            </div>

                            <div class="form-group">
                                <label>Categories</label>
                                <select name="category" class="form-control">
                                    <option  value="">Please select</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"> {{ $category->name }} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>cities</label>
                                <select name="city" class="form-control">
                                    <option value="">Please Select</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" >{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="country">Countries</label>
                                <select name="country" id="" class="form-control">
                                    <option value="">Please Select</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image">File input</label>
                                <input type="file" name="image" id="image">
                                <p class="help-block">Example block-level help text here.</p>
                            </div>

                            <div class="form-group">
                                <label for="sub title">Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                            </div>

                            <div class="form-group">
                                <label for="sub title">Mobile</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile">
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a type="button" class="btn btn-warning" href="{{ route('place.index') }}">Back</a>
                            </div>
                        </div>
                    </div> <!-- box-body  -->
                </form>
            </div> <!-- box primary  -->
        </div>  <!-- /.col-->
    </div>  <!-- ./row -->
    </section>
    <!-- /.content -->
</div> <!-- /.content-wrapper -->


@endsection