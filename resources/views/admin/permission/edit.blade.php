@extends('admin.layouts.app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit title</h3>
                        </div>


                    @include('includes.messages')


                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" action="{{ route('permission.update', $permission->id) }}">
                            <div class="box-body">

                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                <div class="col-lg-offset-3 col-lg-6">

                                    <div class="form-group">
                                        <label for="tag">Permission Title</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Tag title" value="{{ $permission->name }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Permission for</label>
                                        <select class="form-control" name="for" id="for">
                                            <option selected disabled value="">Select Permission for</option>
                                            <option value="super-admin">super-admin</option>
                                            <option value="admin-view">Admin View</option>
                                            <option value="vendor">vendor</option>
                                            <option value="admin">admin</option>
                                            <option value="role">role</option>
                                            <option value="permission">Permission</option>
                                            <option value="advertise">Advertise</option>
                                            <option value="event">Event</option>
                                            <option value="place">Place</option>
                                            <option value="prayer-name">Prayer Name</option>
                                            <option value="prayer-category">Prayer Category</option>
                                            <option value="prayer-sub-category">Prayer Sub Category</option>
                                            <option value="prayer">Prayer</option>
                                            <option value="country">country</option>
                                            <option value="city">city</option>
                                            <option value="category">category</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a type="button" class="btn btn-warning" href="{{ route('permission.index') }}">Back</a>
                                    </div>

                                </div>

                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection