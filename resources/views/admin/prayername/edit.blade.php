@extends('admin.layouts.app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ __('Hamlatik') }}
                <small>{{ __('Admin') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Titles</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('prayername.update', $prayerName->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="box-body">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label for="title"> Name  </label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $prayerName->name }}">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div> <!-- box-body  -->
                        </form>
                    </div> <!-- box primary  -->
                </div>  <!-- /.col-->
            </div>  <!-- ./row -->
        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->

@endsection