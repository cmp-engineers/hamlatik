@extends('admin.layouts.app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hamlatik
                <small>Cities</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Titles</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('city.update', $city->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="title">City Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $city->name }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="image">City Image</label>
                                        <input type="file" name="image" id="image">
                                        <p class="help-block">Image must be less than 150kb</p>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Add Country </label>
                                        <select name="country" id="" class="form-control">
                                            @foreach($cities as $city)
                                                @foreach($countries as $country)
                                                    @if($city->country_id == $country->id)
                                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                    @endif
                                                    @endforeach
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a type="button" class="btn btn-warning " href="{{ route('admin.home') }}">Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection