@extends('admin.layouts.app')

@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection


@section('main-content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{__('Prayer Category')}}
                <small> {{ __('Hamlatik') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('Prayers List') }}</h3>
                    <a class="btn btn-success" href="{{ route('prayersubcategory.create') }}">{{ __('Add Prayer Category') }}</a>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Prayer Category</th>
                                    <th>Prayer Sub Category</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($prayerSubCategories as $prayerSubCategory)

                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>

                                            @foreach($prayerCategories as $prayerCategory)

                                                @if($prayerSubCategory->prayer_categories_id == $prayerCategory->id)

                                                    {{ $prayerCategory->name }}

                                                    @endif

                                                @endforeach


                                        </td>
                                        <td>{{ $prayerSubCategory->name }}</td>
                                        <td><a href="{{ route('prayersubcategory.edit', $prayerSubCategory->id) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
                                        <td>
                                            <form method="post" id="delete-form-{{$prayerSubCategory->id}}" action="{{ route('prayersubcategory.destroy', $prayerSubCategory->id) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>

                                            <a href="" onclick="
                                                    if (confirm('Are you sure, you want to delete this?')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{$prayerSubCategory->id}}').submit();
                                                    } else {
                                                    event.preventDefault();
                                                    }

                                                    ">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th>Prayer Category</th>
                                    <th>Prayer Sub Category</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('footerSection')

    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection
