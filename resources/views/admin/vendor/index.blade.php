@extends('admin.layouts.app')

@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection

@section('main-content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('Vendors List') }}</h3>

                    @can('vendors.create', Auth::user())
                        <a class="btn btn-success" href="{{ route('vendor.create') }}">{{ __('Add Vendor') }}</a>
                    @endcan
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Hamlah Name</th>
                                    <th>Hamlah Admin </th>
                                    <th>mobile</th>
                                    <th>address</th>
                                    <th>Image</th>
                                    @can('vendors.update', Auth::user())
                                        <th>Edit</th>
                                    @endcan

                                    @can('vendors.delete', Auth::user())
                                        <th>Delete</th>
                                    @endcan

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($vendors as $vendor)
                                    @if($vendor->admin_id == auth()->id() || auth()->user()->name == 'Super-admin')
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>{{ $vendor->name }}</td>

                                            @foreach($admins as $admin)
                                                @if($admin->id == $vendor->admin_id)
                                                    <td>{{ $admin->name }}</td>

                                                @endif
                                            @endforeach


                                            <td>{{ $vendor->mobile }}</td>
                                            <td>{{ $vendor->address }}</td>
                                            <td><img class="img-responsive" width="150" height="150" src="{{ Storage::disk('local')->url($vendor->image) }}" alt=""></td>
                                            @can('vendors.update', Auth::user())
                                                <td><a href="{{ route('vendor.edit', $vendor->id) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
                                            @endcan

                                            @can('vendors.delete', Auth::user())
                                                <td>
                                                    <form method="post" id="delete-form-{{$vendor->id}}" action="{{ route('vendor.destroy', $vendor->id) }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>

                                                    <a href="" onclick="
                                                            if (confirm('Are you sure, you want to delete this?')){
                                                            event.preventDefault();
                                                            document.getElementById('delete-form-{{$vendor->id}}').submit();
                                                            } else {
                                                            event.preventDefault();
                                                            }

                                                            ">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </a>

                                                </td>
                                            @endcan
                                        </tr>
                                    @endif
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th>Hamalah Name</th>
                                    <th>Hamlah Admin </th>
                                    <th>mobile</th>
                                    <th>address</th>
                                    <th>Image</th>

                                    @can('vendors.update', Auth::user())
                                        <th>Edit</th>
                                    @endcan

                                    @can('vendors.delete', Auth::user())
                                        <th>Delete</th>
                                    @endcan
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection



@section('footerSection')

    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>

        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection