@extends('admin.layouts.app')


@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hamlatik
                <small>Hamlatik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Titles</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('vendor.update', $vendor->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="box-body">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="title">Hamlah Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $vendor->name }}">
                                    </div>

                                    <div class="form-group">
                                        <label  for="">Admin hamlah</label>
                                        <select class="form-control" name="admin" id="admin">
                                            @foreach($admins as $admin)
                                                <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="Work hours From">Work hours From</label>
                                        <input type="time" class="form-control" id="work_from" name="work_from" value="{{ $vendor->work_from }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="Work hours to">Work hours To</label>
                                        <input type="time" class="form-control" id="work_to" name="work_to"  value="{{ $vendor->work_to }}">
                                    </div>


                                    <div class="form-group">
                                        <label for="Work hours From">Work hours From (Evening)</label>
                                        <input type="time" class="form-control" id="work_from_evening" name="work_from_evening" value="{{ $vendor->work_from_evening }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="Work hours to">Work hours To (Evening)</label>
                                        <input type="time" class="form-control" id="work_to_evening" name="work_to_evening"  value="{{ $vendor->work_to_evening }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="sub title">Mobile</label>
                                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile" value="{{ $vendor->mobile }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="sub title">Address</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="{{ $vendor->address }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug"> Snapchat </label>
                                        <input type="text" class="form-control" id="snapchat" name="snapchat" placeholder="Enter snapchat url" value="{{ $vendor->snapchat }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug"> Instegram </label>
                                        <input type="text" class="form-control" id="instegram" name="instegram" placeholder="Enter instegram url" value="{{ $vendor->instegram }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug"> Twitter </label>
                                        <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Enter Twitter url" value="{{ $vendor->twitter }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug"> Facebook </label>
                                        <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Enter facebook url" value="{{ $vendor->facebook }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug"> hamlah latitude location </label>
                                        <input type="number" class="form-control" id="latitude" name="latitude" placeholder="Enter latitude" value="{{ $vendor->latitude }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug"> hamlah longitude location </label>
                                        <input type="number" class="form-control" id="longitude" name="longitude" placeholder="Enter longitude" value="{{ $vendor->longitude }}">
                                    </div>


                                    <div class="form-group">
                                        <label for="image">File input</label>
                                        <input type="file" name="image" id="image">
                                        <p class="help-block">Example block-level help text here.</p>
                                        <img height="150"  width="150" src="{{ Storage::disk('local')->url($vendor->image) }}" alt="">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a type="button" class="btn btn-warning" href="{{ route('vendor.index') }}">Back</a>
                                    </div>

                                </div>
                            </div> <!-- box-body  -->
                        </form>
                    </div> <!-- box primary  -->
                </div>  <!-- /.col-->
            </div>  <!-- ./row -->
        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->


@endsection



