@extends('admin.layouts.app')


@section('main-content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Hamlat
            <small>Hamlat</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Editors</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Titles</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{ route('advertise.store') }}" enctype="multipart/form-data">
                        <div class="box-body">

                            {{ csrf_field() }}

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="title">Ads Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                                </div>


                              <div class="form-group">
                                <label for="sub title">Mobile</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile">
                              </div>

                            <div class="form-group">
                                    <label for="slug"> Snapchat </label>
                                    <input type="text" class="form-control" id="snapchat" name="snapchat" placeholder="Enter snapchat url">
                            </div>

                            <div class="form-group">
                              <label for="slug"> Instegram </label>
                              <input type="text" class="form-control" id="instegram" name="instegram" placeholder="Enter instegram url">
                            </div>

                            <div class="form-group">
                              <label for="slug"> Twitter </label>
                              <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Enter Twitter url">
                            </div>

                            <div class="form-group">
                              <label for="slug"> Snapchat </label>
                              <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Enter facebook url">
                            </div>

                            <div class="form-group">
                              <label for="slug"> hamlah latitude location </label>
                              <input type="text" step="any" class="form-control" id="latitude" name="latitude" placeholder="Enter latitude">
                            </div>

                            <div class="form-group">
                              <label for="slug"> hamlah longitude location </label>
                              <input type="text" step="any" class="form-control" id="longitude" name="longitude" placeholder="Enter longitude">
                            </div>


                            <div class="form-group">
                              <label for="image">File input</label>
                              <input type="file" name="image" id="image">
                              <p class="help-block">image size must be less than 300kb</p>
                            </div>

                              <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                                </div>

                            </div>
                          </div>
                        </div> <!-- box-body  -->
                    </form>
                </div> <!-- box primary  -->
            </div>  <!-- /.col-->
        </div>  <!-- ./row -->
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->



@endsection
