@extends('admin.layouts.app')

@section('main-content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Hamlat
            <small>Hamlat</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Editors</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Titles</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{ route('advertise.update', $advertise->id) }}" enctype="multipart/form-data">
                        <div class="box-body">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="title">Ads Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $advertise->name }}">
                                </div>


                              <div class="form-group">
                                <label for="sub title">Mobile</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile" value="{{ $advertise->mobile }}">
                              </div>

                            <div class="form-group">
                                    <label for="slug"> Snapchat </label>
                                    <input type="text" class="form-control" id="snapchat" name="snapchat" placeholder="Enter snapchat url" value="{{ $advertise->snapchat }}">
                            </div>

                            <div class="form-group">
                              <label for="slug"> Instegram </label>
                              <input type="text" class="form-control" id="instegram" name="instegram" placeholder="Enter instegram url" value="{{ $advertise->instegram }}">
                            </div>

                            <div class="form-group">
                              <label for="slug"> Twitter </label>
                              <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Enter Twitter url" value="{{ $advertise->twitter }}">
                            </div>

                            <div class="form-group">
                              <label for="slug"> Facebook </label>
                              <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Enter facebook url" value="{{ $advertise->facebook }}">
                            </div>

                            <div class="form-group">
                              <label for="slug"> hamlah latitude location </label>
                              <input type="text" class="form-control" id="latitude" name="latitude" placeholder="Enter latitude" value="{{ $advertise->latitude }}">
                            </div>

                            <div class="form-group">
                              <label for="slug"> hamlah longitude location </label>
                              <input type="text" class="form-control" id="longitude" name="longitude" placeholder="Enter longitude" value="{{ $advertise->longitude }}">
                            </div>


                            <div class="form-group">
                              <label for="image">File input</label>
                              <input type="file" name="image" id="image">
                              <p class="help-block">Example block-level help text here.</p>
                                <img class="img-responsive" width="150" height="150" src="{{ Storage::disk('local')->url($advertise->image) }}" alt="">
                            </div>

                              <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                                  <a  class="btn btn-warning" href="{{ route('advertise.index') }}">Back</a>
                                </div>

                            </div>
                          </div>
                        </div> <!-- box-body  -->
                    </form>
                </div> <!-- box primary  -->
            </div>  <!-- /.col-->
        </div>  <!-- ./row -->
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->


@endsection
