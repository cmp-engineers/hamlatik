<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src=" {{ asset('admin/dist/img/hamlatik-logo.png') }} " class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Hamlatik.com</p>

            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Offer</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('offer.create') }}"><i class="fa fa-circle-o"></i> Add Offer</a></li>
                    <li><a href=" {{ route('offer.index') }} "><i class="fa fa-circle-o"></i> View Offers</a></li>
                </ul>
            </li>

            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Vendor</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @can('vendors.admin', Auth::user())
                        <li class="active"><a href="{{  route('vendor.create') }}"><i class="fa fa-circle-o"></i> Add Vendor</a></li>
                    @endcan
                    <li><a href="{{ route('vendor.index') }}"><i class="fa fa-circle-o"></i> View Vendors</a></li>

                </ul>
            </li>


            @can('vendors.admin', Auth::user())

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-group"></i> <span>Admin</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('user.create') }}"><i class="fa fa-circle-o"></i> Add Admin</a></li>
                        <li><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i> View Admins</a></li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-group"></i> <span>Role</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('role.create') }}"><i class="fa fa-circle-o"></i> Add Role</a></li>
                        <li><a href="{{ route('role.index') }}"><i class="fa fa-circle-o"></i> View Roles</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-group"></i> <span>Permission</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('permission.create') }}"><i class="fa fa-circle-o"></i> Add Permission</a></li>
                        <li><a href="{{ route('permission.index') }}"><i class="fa fa-circle-o"></i> View Permissions</a></li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Advertise</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('advertise.create') }}"><i class="fa fa-circle-o"></i> Add Ads</a></li>
                        <li><a href="{{ route('advertise.index') }}"><i class="fa fa-circle-o"></i> View Advertise</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-calendar-minus-o"></i> <span>Event</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('event.create') }}"><i class="fa fa-circle-o"></i> Add Event</a></li>
                        <li><a href="{{ route('event.index') }}"><i class="fa fa-circle-o"></i> View Event</a></li>
                    </ul>
                </li>



                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-tripadvisor"></i> <span>Place</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('place.create') }}"><i class="fa fa-circle-o"></i> Add Place</a></li>
                        <li><a href="{{ route('place.index') }}"><i class="fa fa-circle-o"></i> View places</a></li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Prayer Name</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('prayername.create') }}"><i class="fa fa-circle-o"></i> Add Prayer name</a></li>
                        <li><a href="{{ route('prayername.index') }}"><i class="fa fa-circle-o"></i> View prayers names</a></li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Prayer Category</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('prayercategory.create') }}"><i class="fa fa-circle-o"></i> Add Prayer Category </a></li>
                        <li><a href="{{ route('prayercategory.index') }}"><i class="fa fa-circle-o"></i> View prayers Categories</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Prayer Sub Category</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('prayersubcategory.create') }}"><i class="fa fa-circle-o"></i> Add Prayer Sub Category</a></li>
                        <li><a href="{{ route('prayersubcategory.index') }}"><i class="fa fa-circle-o"></i> View prayers sub Categories</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Prayer</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('prayer.create') }}"><i class="fa fa-circle-o"></i> Add Prayer</a></li>
                        <li><a href="{{ route('prayer.index') }}"><i class="fa fa-circle-o"></i> View prayers</a></li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-flag"></i> <span>Country</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('country.create') }}"><i class="fa fa-circle-o"></i> Add Country</a></li>
                        <li><a href=" {{ route('country.index') }}"><i class="fa fa-circle-o"></i> View Countries</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>City</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('city.create') }}"><i class="fa fa-circle-o"></i> Add City</a></li>
                        <li><a href=" {{ route('city.index') }}"><i class="fa fa-circle-o"></i> View cities</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Category</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{ route('category.create') }}"><i class="fa fa-circle-o"></i> Add Category</a></li>
                        <li><a href="{{ route('category.index') }}"><i class="fa fa-circle-o"></i> View Categories</a></li>
                    </ul>
                </li>
            @endcan
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
