@extends('admin.layouts.app')

@section('main-content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hamlatik
                <small>{{ __('Prayers') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Prayers</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('prayer.update', $prayer->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="box-body">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label for="prayername">Name</label>
                                        <select name="prayername" id="" class="form-control">
                                            <option value="" disabled>{{ __('Please select') }}</option>
                                            @foreach($prayerNames as $prayerName)
                                                <option value="{{ $prayerName->id }}"
                                                @if($prayer->prayer_names_id == $prayerName->id)
                                                    selected
                                                    @endif
                                                > {{ $prayerName->name }}</option>
                                                @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="prayercategory">Prayer Category</label>
                                        <select name="prayercategory" id="" class="form-control">
                                            <option value="" disabled >{{ __('Please select') }}</option>
                                            @foreach($prayerCategories as $prayerCategory)
                                                <option  value="{{ $prayerCategory->id }}"
                                                         @if($prayer->prayer_categories_id == $prayerCategory->id)
                                                             selected
                                                                 @endif
                                            > {{ $prayerCategory->name }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="praysubcategory">Prayer Sub Category</label>
                                        <select name="prayersubcategory" id="" class="form-control">
                                            <option value="" disabled >{{ __('Please select') }}</option>
                                            @foreach($prayerSubCategories as $prayerSubCategory)

                                                <option  value="{{ $prayerSubCategory->id }}"

                                                         @if($prayer->prayer_sub_categories_id == $prayerSubCategory->id)
                                                         selected
                                                        @endif

                                                > {{ $prayerSubCategory->name }}</option>

                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="sub title">Category Text</label>
                                        <textarea rows="5" cols="80" type="text" class="form-control" id="editor1" name="prayertext">
                                        {{ $prayer->prayertext }}
                                    </textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a class="btn btn-warning" href="{{ route('prayer.index') }}">Back</a>
                                    </div>
                                </div>
                            </div> <!-- box-body  -->
                        </form>
                    </div> <!-- box primary  -->
                </div>  <!-- /.col-->
            </div>  <!-- ./row -->
        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->

@endsection


@section('footerSection')

    <script src="{{ asset('admin/plugins/select2/select2.full.js') }}"></script>
    <script>
        $(document).ready(function () {
           $(".select2").select2();
        });
    </script>

    <!-- CK Editor -->
    <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>

    <!-- AdminLTE for demo purpose -->
    <script src="{{ asset('admin/dist/js/demo.js') }}"></script>

    <script>
        $(function () {
           // Replace the <textarea id="editor1"> with a CKEDitor
            // instance , using default configuration
            CKEDITOR.replace('editor1');
            // boostrap WYSIHTML5 - Text editor
            $(".textarea").wysihtml5();
        });
    </script>

@endsection
