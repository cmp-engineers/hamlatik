@extends('admin.layouts.app')


@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Admin</h3>
                        </div>

                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('admin.profile-update', $admin->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="box-body">
                                <div class="col-lg-12 ">

                                    <div class="form-group">
                                        <label for="title">username</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter user name" value="@if(old('name')){{ old('name') }}@else{{ $admin->name }}@endif">
                                    </div>
                                    <div class="form-group">
                                        <label for="sub title">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" value="@if(old('email')){{old('email')}}@else{{$admin->email}}@endif">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">phone</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Mobile number" value="@if(old('phone')){{ old('phone')}}@else{{$admin->phone}}@endif">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" >
                                    </div>

                                    <div class="form-group">
                                        <label for="confirm password">Confirm Password</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                                    </div>
                                </div>


                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a type="button" class="btn btn-warning" href="{{ route('admin.home') }}">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection