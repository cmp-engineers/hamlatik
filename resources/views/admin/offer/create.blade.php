@extends('admin.layouts.app')


{{-- offers is for hamlah --}}
@section('main-content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hamlatik
                <small>Hamlatik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Titles</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('offer.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="title">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                                    </div>


                                    @can('vendors.view', Auth::user())
                                    <div class="form-group">
                                        <label for="">Select Vendor</label>
                                        <select  name="vendor" class="form-control"> Select Vendor
                                            @foreach($vendors as $vendor)
{{--                                                @foreach($admins as $admin)--}}
{{--                                                    @if($admin->id == auth()->id() && $vendor->admin_id == $admin->id  )--}}
                                                       @if($vendor->admin_id == auth()->id())
                                                        <option value="{{ $vendor->id }}"  > {{$vendor->name}}</option>
                                                        @endif
                                                @endforeach
{{--                                            @endforeach--}}

                                        </select>
                                    </div>
                                    @endcan

                                    @can('vendors.admin', Auth::user())

                                    <div class="form-group">
                                        <label for="vendor">Select Vendor</label>
                                        <select name="vendor" id="" class="form-control">
                                            @foreach($vendors as $vendor)

                                                <option value="{{ $vendor->id }}"> {{ $vendor->name }}</option>
                                             @endforeach
                                        </select>
                                    </div>
                                    @endcan



                                    <div class="form-group">
                                        <label for="image">Offer </label>
                                        <input type="file" name="image" id="image">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>

                                </div>
                            </div> <!-- box-body  -->
                        </form>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



@endsection