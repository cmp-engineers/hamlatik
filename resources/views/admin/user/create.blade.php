@extends('admin.layouts.app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Admin</h3>
                        </div>

                    @include('includes.messages')
                    <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('user.store') }}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="col-lg-12 ">

                                    <div class="form-group">
                                        <label for="title">username</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter user name" value="{{ old('name') }}">
                                    </div>


                                    <div class="form-group">
                                        <label for="sub title">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">phone</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Mobile number" value="{{ old('phone') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" value="{{ old('password') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="confirm password">Confirm Password</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                                    </div>

                                    <div class="form-group">
                                        <label for=status">Status</label>
                                        <div class="checkbox">
                                            <label for="roles"><input type="checkbox" name="status" @if(old('status') == 1)
                                                checked
                                                                      @endif value="1">{{ __('Status') }}</label>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label for="roles">Assign Role</label>
                                    <div class="rows">
                                        @foreach($roles as $role)

                                            <div class="col-lg-3">
                                                <div class="checkbox">
                                                    <label for="roles"><input type="checkbox" name="role[]" value="{{ $role->id }}">{{ $role->name }}</label>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                </div>


                            </div>

                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a type="button" class="btn btn-warning" href="{{ route('user.index') }}">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection