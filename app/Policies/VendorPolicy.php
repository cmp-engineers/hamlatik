<?php

namespace App\Policies;

use App\Model\Admin\Role;
use App\Model\User\User;
use App\Model\Admin\Vendor;
use App\Model\Admin\Admin;
use App\Model\Admin\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class VendorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the vendor.
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */
    public function view(Admin $admin)
    {
        //
        return $this->getPermissionVendor($admin,'vendor-view' );

//        $permission = new Permission();
//        $role = new Role();

//        if ($permission->name == 'vendor-view' || $permission->name == 'super-admin') {
//            return true;
//        } else {
//            return false;
//        }

//        if ($role->name == 'vendor') {
//            return true;
//        } else {
//            return false;
//        }
    }

    /**
     * Determine whether the user can create vendors.
     *
     * @param  \App\Model\User\User  $user
     * @return mixed
     */
    public function create(Admin $admin)
    {
        //
        return $this->getPermission($admin,3);
    }

    /**
     * Determine whether the user can update the vendor.
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */
    public function update(Admin $admin)
    {
        //

        return $this->getPermission($admin,4);
    }

    /**
     * Determine whether the user can delete the vendor.
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */
    public function delete(Admin $admin)
    {
        //





        return $this->getPermission($admin, 5);
    }

    /**
     * Determine whether the user can restore the vendor.
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */
    public function restore(User $user, Vendor $vendor)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the vendor.
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */
    public function forceDelete(User $user, Vendor $vendor)
    {
        //
    }

    /**
     * Admin function
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */

    public function admin(Admin $admin)
    {
        return $this->getPermission($admin, 1);
    }

    public function category(Admin $admin)
    {
        return $this->getPermission($admin,15);
    }


    /**
     * Passing a function getPermission.
     *
     * @param  \App\Model\User\User  $user
     * @param  \App\Vendor  $vendor
     * @return mixed
     */

    protected function getPermission($admin, $p_id)
    {
        foreach ($admin->roles as $role)
        {
            foreach ($role->permissions as $permission)
            {
                if ($permission->id == $p_id || $permission->name == 'super-admin')
                {
                    return true;
                }
            }
        }
        return false;
    }

    protected function getPermissionVendor($admin, $p_name) {

        foreach ($admin->roles as $role)
        {
            foreach ($role->permissions as $permission) {
                if ($permission->name == $p_name) {
                    return true;
                }
            }
        }

        return false;
    }
}
