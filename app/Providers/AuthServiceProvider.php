<?php

namespace App\Providers;

use App\Model\admin\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Model\Admin\Vendor' => 'App\Policies\VendorPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::resource('vendors', 'App\Policies\VendorPolicy');
        Gate::define('vendors.admin','App\Policies\VendorPolicy@admin');
        Gate::define('vendors.category','App\Policies\VendorPolicy@category');
        Gate::define('vendors.view', 'App\Policies\VendorPolicy@view');


//        Gete::define('vendor-view' , function (){
//
//            if (Permission::where('name' , 'vendor-view')) {
//                return true;
//            } else {
//                return false;
//            }
//
//        });

    }
}
