<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Admin;
use App\Model\Admin\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vendors = Vendor::all();
        $admins = Admin::all();
        return view('admin.vendor.index', compact('vendors','admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $admins = Admin::all();
        return view('admin.vendor.create', compact('admins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        $this->validate($request, [
//
//            'name' => 'required',
//            'work_from' => 'required',
//            'work_to' => 'required',
//            'mobile' => 'required',
//            'address' => 'required',
//
//        ]);



        $vendor  = new Vendor;

        if ($request->name != null) {
            $vendor->name = $request->name;
        }

       if ($request->work_from != null) {
           $vendor->work_from = $request->work_from;
       }

       if ($request->work_to != null) {
           $vendor->work_to    = $request->work_to;
       }


       if ($request->work_from_evening != null) {
           $vendor->work_from_evening = $request->work_from_evening;

       }

       if($request->work_to_evening != null) {
           $vendor->work_to_evening = $request->work_to_evening;
       }


       if ($request->mobile != null){
           $vendor->mobile     = $request->mobile;
       } else {
           $vendor->mobile = 'لا يوجد رقم هاتف' ;
       }

       if ($request->address != null){
           $vendor->address    = $request->address;

       } else {
           $vendor->address = 'لا يوجد عنوان';
       }

        if ($request->snapchat != null) {

            $vendor->snapchat = $request->snapchat;
        } else {

            $vendor->snapchat = 'www.snapchat.com';
        }

        if ($request->instegram != null){

        $vendor->instegram  = $request->instegram;

        } else {
             $vendor->instegram  = 'www.instegram.com';
        }

        if ($request->twitter != null){
            $vendor->twitter    = $request->twitter;
        } else {
            $vendor->twitter = 'www.twitter.com';
        }

        if ($request->facebook != null) {
            $vendor->facebook   = $request->facebook;
        } else {
            $vendor->facebook = 'www.facebook.com';
        }

        if ($request->latitude != null) {
            $vendor->latitude   = $request->latitude;
        } else {
            $vendor->latitude  = '0.0000000';
        }

        if ($request->longitude != null) {
            $vendor->longitude  = $request->longitude;
        } else {
            $vendor->longitude = '0.00000';
        }


        if ($request->hasFile('image')) {
            $vendorImage =  $request->image->store('public/images');
            $vendor->image      = $vendorImage;
        } else {

            $vendor->image = 'No image';

        }

        if ($request->admin != null) {
            $vendor->admin_id = $request->admin;
        }


        $vendor->save();

        return redirect(route('vendor.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

      //  if (Auth::user()->can('vendors.update') || Auth::user()->name == 'Super-admin')
      //  {
            $vendor = Vendor::find($id);
            $admins = Admin::all();
            return view('admin.vendor.edit', compact('vendor','admins'));
        //}

       // return redirect(route('admin.home'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $vendor = Vendor::find($id);

        $vendor->name = $request->name;
        $vendor->work_from = $request->work_to;
        $vendor->work_to   = $request->work_from;
        $vendor->work_from_evening = $request->work_from_evening;
        $vendor->work_to_evening = $request->work_to_evening;
        $vendor->mobile     = $request->mobile;
        $vendor->address = $request->address;
        $vendor->snapchat  = $request->snapchat;
        $vendor->instegram = $request->instegram;
        $vendor->twitter = $request->twitter;
        $vendor->facebook = $request->facebook;
        $vendor->latitude =  $request->latitude;
        $vendor->longitude = $request->longitude;

        if ($request->hasFile('image')) {
            $vendorImage = $request->image->store('public/images');
            $vendor->image = $vendorImage;
        }



        $vendor->save();

        return redirect(route('vendor.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Vendor::where('id',$id)->delete();

        return redirect()->back();
    }
}
