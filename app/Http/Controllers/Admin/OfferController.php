<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Admin;
use App\Model\Admin\Offer;
use App\Model\admin\Permission;
use App\Model\Admin\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfferController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $offers = Offer::orderBy('id','DESC')->get();
        $admins = Admin::all();
        $vendors = Vendor::all();
        return view('admin.offer.index',compact('offers','admins','vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $vendors = Vendor::all();
        $admins = Admin::all();
        $permission = Permission::where('name', 'Super-admin');
        return view('admin.offer.create', compact('vendors', 'admins', 'permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'image' => 'required',
        ]);


        if ($request->hasFile('image')) {
         $offerImage =   $request->image->store('public/images');
        }

        $offer = new Offer;
        $offer->name = $request->name;
        $offer->image = $offerImage;
        $offer->vendor_id = $request->vendor;
        $offer->admin_id = auth()->id();


        $offer->save();

        $vendor = new Vendor;
        $vendor->id = $request->vendor;

        $offer->vendor()->associate($vendor->id)->save();



        return redirect(route('offer.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $offer = Offer::where('id',$id)->first();
        $vendor = Vendor::all();
        return view('admin.offer.edit', compact('offer', 'vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
                'name' => 'required',

            ]);




        $offer = Offer::find($id);

        $offer->name = $request->name;


        if ($request->hasFile('image')){
            $offerImage =  $request->image->store('public/images');
            $offer->image = $offerImage;
        }

        $offer->save();

        return redirect(route('offer.index'));




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Offer::where('id',$id)->delete();
        return redirect()->back();
    }
}
