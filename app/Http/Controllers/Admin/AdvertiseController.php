<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Advertise;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\File;


class AdvertiseController extends Controller
{

    public function __construct()
    {
         $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $advertises = Advertise::all();
        return view('admin.advertise.index', compact('advertises'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.advertise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//       $this->validate($request, [
//
//           'name' => 'required',
//           'mobile' => 'required',
//
//       ]);


//       $inputs = $request->all();
//
//       if ($file = $request->file('image')) {
//           $name = $file->getClientOriginalName();
//           $file->move('public/images', $name);
//           $inputs['images'] = $name;
//       }
//
//       Advertise::create($inputs);

        if ($request->hasFile('image')) {
        $advertiseImage =  $request->image->store('public/images');
    }

//        if ($request->hasFile('image')) {
//
//            $image = $request->file('image');
//            $advertiseImage = time() . '.' . $image->getClientOriginalName();
//            Image::make($image)->resize(300,300)->save(storage_path('public/images' . $advertiseImage));
//
//        }

        $advertise = new Advertise;

        $advertise->image = $advertiseImage;
       $advertise->name     = $request->name;
       $advertise->mobile   = $request->mobile;
       $advertise->snapchat = $request->snapchat;
       $advertise->instegram = $request->instegram;
       $advertise->twitter = $request->twitter;
       $advertise->facebook = $request->facebook;
       $advertise->latitude = $request->latitude;
       $advertise->longitude = $request->longitude;

       $advertise->save();

       return redirect(route('advertise.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $advertise = Advertise::where('id', $id)->first();
        return view('admin.advertise.edit', compact('advertise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name',
            'mobile',

        ]);

        $advertise = Advertise::find($id);

        if ($request->hasFile('image') != null) {
         $advertiseImage = $request->image->store('public/images');
            $advertise->image        =  $advertiseImage;
        }


        $advertise->name        = $request->name;
        $advertise->mobile      = $request->mobile;
        $advertise->snapchat    = $request->snapchat;
        $advertise->instegram   = $request->instegram;
        $advertise->twitter     = $request->twitter;
        $advertise->facebook    = $request->facebook;
        $advertise->latitude    = $request->latitude;
        $advertise->longitude    = $request->longitude;

        $advertise->save();

        return redirect(route('advertise.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Advertise::where('id',$id)->delete();

        redirect()->back();

    }
}
