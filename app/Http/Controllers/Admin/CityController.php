<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\City;
use App\Model\Admin\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cities = City::all();
        $countries = Country::all();
        return view('admin.city.index', compact('cities', 'countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::all();
        return view('admin.city.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(City $city)
    {
        //
        $this->validate(request(),[
            'name' => 'required',
            'image' => 'required',
            'country' => 'required',

        ]);

        $city->name = request('name');
        $city->country_id = request('country');
        $city->image = request('image')->store('public/images');
        $city->save();

        return redirect(route('city.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = City::where('id', $id)->first();
        $countries = Country::all();
        $cities = City::all();
        return view('admin.city.edit', compact('city', 'countries','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(City $city)
    {
        //
        $this->validate(request(), [
            'name'=> 'required',
        ]);


        $city->name = request('name');
        $city->country_id = request('country');

        if (request()->hasFile('image') != null) {
            $city->image = request('image')->store('public/images');
        }

        $city->update();


        return redirect(route('city.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        City::where('id',$id)->delete();
        return redirect()->back();
    }
}
