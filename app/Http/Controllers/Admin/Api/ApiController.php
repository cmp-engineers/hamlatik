<?php

namespace App\Http\Controllers\Admin\Api;

use App\Model\Admin\Advertise;
use App\Model\Admin\Category;
use App\Model\Admin\City;
use App\Model\Admin\Country;
use App\Model\Admin\Event;
use App\Model\Admin\Offer;
use App\Model\Admin\Place;
use App\Model\Admin\Prayer;
use App\Model\Admin\PrayerCategory;
use App\Model\Admin\PrayerName;
use App\Model\Admin\PrayerSubCategory;
use App\Model\Admin\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{



    // for all hamlat
    public function vendors()
    {
        return Vendor::all();
    }

    // for a specific hamlah
    public function vendor($id)
    {
        return Vendor::where('id',$id)->get();
    }

    public function advertises()
    {
        return Advertise::all();
    }

    public function advertise($id)
    {
        return Advertise::where('id', $id)->get();
    }

    public function categories()
    {
        return Category::all();
    }

    public function category($id)
    {
        return Category::where('id',$id)->get();
    }

    public function cities()
    {
        return City::all();
    }

    public function city($id)
    {
        return City::where('id',$id)->get();
    }

    public function countries()
    {
        return Country::all();
    }

    public function country($id)
    {
        return Country::where('id', $id)->get();
    }

    public function citiesByCountry($country_id)
    {
        return City::where(['country_id' => $country_id])->get();
    }

   public function events()
   {
       return Event::all();
   }

   public function event($id)
   {
       return Event::where('id',$id)->get();
   }

   public function offers()
   {
       //return Offer::all()->sortBy('DESC');

       return Offer::orderBy('id','DESC')->get();
       //return 'test';
   }


   public function offer($id)
   {
       return Offer::where('id',$id)->orderBy('id','DESC')->get();
   }

   public function offerByVendor(Request $request)
   {
       $input = $request->all();
       return Offer::where(['id' => $input['id'], 'vendor_id' => $input['vendor_id']])->orderBy('id','DESC')->get();
       //return Offer::where(['vendor_id' => $input['vendor_id']])->get();
   }

   public function places()
   {
       return Place::all();
   }

   public function place($id)
   {
       return Place::where('id',$id)->get();
   }

   // post method
//   public function placeByCategoryCity(Request $request)
//   {
//       $input = $request->all();
//       return Place::where(['city_id' => $input['city_id'] , 'category_id' => $input['category_id']])->get();
//   }

//    public function placeByCategoryCountryCity(Request $request)
        public function  placeByCountryCityCategory($country_id,$city_id,$category_id)
         //   public function placeByCategoryCountryCity()
    {
        //$input = $request->all();
        return Place::where(['category_id' => $category_id, 'country_id' => $country_id, 'city_id' => $city_id])->get();
        //return Place::where(['category_id' => $category_id , 'city_id' => $city_id,'country_id' => $country_id  ])->get();
       // return Place::all();
    }

   public function prayers()
   {
       return Prayer::all();
   }

   public function prayerByNameCategorySubCategory($id1,$id2,$id3)
   {
       return Prayer::where([ 'prayer_names_id' => $id1 ,  'prayer_categories_id' => $id2,  'prayer_sub_categories_id' => $id3])->get();
   }

   public function prayerCategories()
   {
       return PrayerCategory::all();
   }

   public function prayerCategory(Request $request)
   {
       $input = $request->all();

       return PrayerCategory::where(['id' => $input['prayer_category_id']])->get();
   }


   public function prayerNamesCategoryPrayers(Request $request)
   {
       $input = $request->all();

       return Prayer::where(['prayer_categories_id' => $input['prayer_categories_id'] ])->get();
   }


   // fetch me all data that has prayer_names_id
   public function prayerCategoryByName(Request $request)
   {
       $input = $request->all();

       return PrayerCategory::where(['prayer_names_id' => $input['prayer_names_id'] ])->get();
   }

   public function prayerSubCategories()
   {
       return PrayerSubCategory::all();
   }

//   public function prayerSubCategory($id)
//   {
//       return PrayerSubCategory::where('id', $id)->get();
//   }

  public function prayerSubCategory($prayer_categories_id)
  {
      return PrayerSubCategory::where('prayer_categories_id', $prayer_categories_id)->get();
  }



   public function PrayerCategoryPrayerSubCategory($id1,$id2)
   {
       return PrayerSubCategory::where(['id' =>  $id1, 'prayer_categories_id' => $id2])->get();
   }

   public function prayerNames()
   {
       return PrayerName::all();
   }

   public function prayerName($id)
   {
       return PrayerName::where('id', $id)->get();
   }

}

