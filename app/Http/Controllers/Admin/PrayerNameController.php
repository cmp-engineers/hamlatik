<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\PrayerName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrayerNameController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $prayerNames = PrayerName::all();
        return view('admin.prayername.index', compact('prayerNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.prayername.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'name' => 'required',
        ]);

        $prayerName = new PrayerName;
        $prayerName->name = $request->name;
        $prayerName->save();

        return redirect(route('prayername.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $prayerName = PrayerName::find($id);
       return view('admin.prayername.edit', compact('prayerName'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[

            'name' => 'required'

        ]);

        $prayerName = PrayerName::find($id)->first();

        $prayerName->name = $request->name;
        $prayerName->save();

        return redirect(route('prayername.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        PrayerName::where('id',$id)->delete();
        return redirect()->back();
    }
}
