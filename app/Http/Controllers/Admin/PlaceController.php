<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Category;
use App\Model\Admin\City;
use App\Model\Admin\Country;
use App\Model\Admin\Place;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlaceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $places         = Place::all();
        $cities         = City::all();
        $categories     = Category::all();
        $countries      = Country::all();
        return view('admin.place.index', compact('places','cities','categories','countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cities     = City::all();
        $categories = Category::all();
        $countries  = Country::all();

        return view('admin.place.create', compact('cities','categories','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Place $place)
    {


        $this->validate(request(),[

            'category' => 'required',
            'city' => 'required',
            'country' => 'required',
            'image' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'name' => 'required',

        ]);


        $place->name             = request('name');
        $place->category_id      = request('category');
        $place->city_id          = request('city');
        $place->country_id       = request('country');
        $place->address          = request('address');
        $place->mobile           = request('mobile');


        if (request()->hasFile('image')) {
            $place->image = request('image')->store('public/images');
        }

        $place->save();

        return redirect(route('place.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cities     =  City::all();
        $categories =  Category::all();
        $countries  =  Country::all();
        $place = Place::where('id',$id)->first();

        return view('admin.place.edit',compact('cities','categories','place', 'countries'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Place $place)
    {
        //

        $place->name              = request('name');
        $place->category_id       = request('category');
        $place->country_id        = request('country');
        $place->city_id           = request('city');
        $place->address           = request('address');
        $place->mobile            = request('mobile');

        if (request()->hasFile('image') != null) {
            $place->image = request('image')->store('public/images');
        }

        $place->update();

        return redirect(route('place.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Place::where('id',$id)->delete();

        return redirect()->back();

    }
}
