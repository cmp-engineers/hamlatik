<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\PrayerCategory;
use App\Model\Admin\PrayerSubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrayerSubCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $prayerCategories = PrayerCategory::all();
        $prayerSubCategories = PrayerSubCategory::all();

        return view('admin.prayersubcategory.index', compact('prayerSubCategories', 'prayerCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $prayerCategories = PrayerCategory::all();
        return view('admin.prayersubcategory.create', compact('prayerCategories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'name' => 'required',

        ]);

        $prayerSubCategory = new PrayerSubCategory;

        $prayerSubCategory->name = $request->name;
        $prayerSubCategory->prayer_categories_id = $request->prayercategory;

        $prayerSubCategory->save();

        return redirect(route('prayersubcategory.index'));



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $prayerCategories = PrayerCategory::all();
        $prayerSubCategory = PrayerSubCategory::where('id',$id)->first();
        return view('admin.prayersubcategory.edit',compact('prayerSubCategory', 'prayerCategories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $prayerSubCategory = PrayerSubCategory::find($id);

        $prayerSubCategory->name = $request->name;
        $prayerSubCategory->prayer_categories_id = $request->prayercategory;

        $prayerSubCategory->save();

        return redirect(route('prayersubcategory.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        PrayerSubCategory::where('id',$id)->delete();

        return redirect()->back();
    }
}
