<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Prayer;
use App\Model\Admin\PrayerCategory;
use App\Model\Admin\PrayerName;
use App\Model\Admin\PrayerSubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrayerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $prayers = Prayer::all();
        $prayerNames = PrayerName::all();
        $prayerCategories = PrayerCategory::all();
        $prayerSubCategories = PrayerSubCategory::all();
        return view('admin.prayer.index', compact('prayers', 'prayerCategories','prayerSubCategories','prayerNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $prayerCategories = PrayerCategory::all();
        $prayerSubCategories = PrayerSubCategory::all();
        $prayerNames = PrayerName::all();
        return view('admin.prayer.create', compact('prayerCategories','prayerSubCategories', 'prayerNames'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'prayername' => 'required',
            'prayercategory' => 'required',
            'prayertext' => 'required',

        ]);

        $prayer = new Prayer;

        $prayer->prayer_names_id                       = $request->prayername;
        $prayer->prayer_categories_id                  = $request->prayercategory;

        if ($request->prayersubcategory == null)
        {
            $prayer->prayer_sub_categories_id         = $request->prayersubcategory = 0;
        }

        //$prayer->prayertext                            =  trim(strip_tags($request->prayertext));
       // $prayer->prayertext                             = html_entity_decode($request->prayertext);
        $prayer->prayertext                                 = html_entity_decode(strip_tags($request->prayertext));
       /* preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $request->prayertext); */

        $prayer->save();

        return redirect(route('prayer.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $prayer = Prayer::find($id);
        $prayerNames = PrayerName::all();
        $prayerCategories = PrayerCategory::all();
        $prayerSubCategories = PrayerSubCategory::all();



        return view('admin.prayer.edit', compact('prayer', 'prayerCategories','prayerSubCategories', 'prayerNames'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $prayer = Prayer::find($id);

        $prayer->prayer_names_id                         = $request->prayername;
        $prayer->prayer_categories_id                    = $request->prayercategory;
        $prayer->prayer_sub_categories_id                = $request->prayersubcategory;
        $prayer->prayertext                              = $request->prayertext;

        $prayer->save();

        return redirect(route('prayer.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Prayer::where('id',$id)->delete();
        return redirect()->back();
    }
}
