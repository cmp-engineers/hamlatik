<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\PrayerCategory;
use App\Model\Admin\PrayerName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrayerCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $prayerCategories  =  PrayerCategory::all();
       $prayerNames = PrayerName::all();

       return view('admin.prayercategory.index', compact('prayerCategories', 'prayerNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $prayerNames = PrayerName::all();
        return view('admin.prayercategory.create', compact('prayerNames'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

           'name' => 'required'
        ]);

        $prayerCategory =  new PrayerCategory;

        $prayerCategory->name = $request->name;
        $prayerCategory->prayer_names_id = $request->prayername;


        // here we dont want to send anything unless has value
        if($request->hassubcategory != null){
            $prayerCategory->hasSubCategory = $request->hassubcategory;
        }


       // return    $prayerCategory->hasSubCategory = $request->hassubcategory;

        //$hassubcategory = $request->hassubcategory;

//         if ($hassubcategory == null)
//         {
//             $hassubcategory = 0;
//         }

      //  $prayerCategory->hasSubCategory = $hassubcategory;
        $prayerCategory->save();

        return redirect(route('prayercategory.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $prayerCategory = PrayerCategory::where('id', $id)->first();
        $prayerNames = PrayerName::all();
        return view('admin.prayercategory.edit',compact('prayerCategory', 'prayerNames'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[

            'name' => 'required'

        ]);

        // where cant use later on save , but find yes can
       // $prayerCategory = PrayerCategory::where('id',$id);
        $prayerCategory = PrayerCategory::find($id);

        $prayerCategory->name = $request->name;
        $prayerCategory->prayer_names_id = $request->prayername;

        $prayerCategory->save();

        return redirect(route('prayercategory.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        PrayerCategory::where('id',$id)->delete();

        return redirect()->back();
    }
}
