<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Advertise extends Model
{
    //
    protected $fillable = [

        'name',
        'mobile',
        'snapchat',
        'instegram',
        'twitter',
        'facebook',
        'latitude',
        'longitude',
        'image'
    ];
}
