<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

   // protected $guarded = [ ''];

    protected  $fillable = [ 'name' , 'permission[]'];

    //protected $guarded = array();

    public function permissions()
    {
        return $this->belongsToMany('App\Model\admin\Permission');
    }
}
