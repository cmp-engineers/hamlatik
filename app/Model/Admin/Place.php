<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{

    protected $fillable = [
        'name',
        'image'
    ];

    //
    public function cities()
    {
        return $this->hasOne(City::class);
    }

    public function categories()
    {
        return $this->hasOne(Category::class);
    }

    public function countries()
    {
        return $this->hasOne(Country::class);
    }


}
