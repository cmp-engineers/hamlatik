<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    //
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
