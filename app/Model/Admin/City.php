<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

}
