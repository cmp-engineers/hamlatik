<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

}
