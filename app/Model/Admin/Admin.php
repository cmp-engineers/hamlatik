<?php

namespace App\Model\Admin;

use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $fillable = [

        'name', 'email', 'password' , 'status' , 'phone'

    ];

    protected $hidden = [

        'password' , 'remember_token',

    ];

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Model\Admin\Role');
    }

}
