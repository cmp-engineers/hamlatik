<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('admin_id');
            $table->time('work_from');
            $table->time('work_to');
            $table->time('work_from_evening');
            $table->time('work_to_evening');
            $table->string('mobile');
            $table->text('address');
            $table->string('snapchat');
            $table->string('instegram');
            $table->string('twitter');
            $table->string('facebook');
            $table->text('image');
            $table->decimal('latitude');
            $table->decimal('longitude');
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
