<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\Model\Admin\Vendor::class,20)->create();
        factory(App\Model\Admin\Advertise::class,5)->create();
        factory(App\Model\Admin\Event::class,20)->create();
        factory(App\Model\Admin\Offer::class,20)->create();
        factory(App\Model\Admin\Place::class,20)->create();
        factory(App\Model\Admin\Prayer::class,20)->create();
        factory(App\Model\Admin\PrayerCategory::class,20)->create();
        factory(App\Model\Admin\PrayerName::class,20)->create();
        factory(App\Model\Admin\PrayerSubCategory::class,20)->create();

    }
}
