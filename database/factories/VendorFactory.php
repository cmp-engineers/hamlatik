<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Admin\Vendor::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'work_from' => $faker->time('H:i:s'),
        'work_to' => $faker->time('H:i:s'),
        'mobile' => $faker->phoneNumber,
        'address' => $faker->streetAddress,
        'snapchat' => $faker->url,
        'instegram' => $faker->url,
        'twitter' => $faker->url,
        'facebook' => $faker->url,
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'image' => $faker->image('public/storage/images', $width = 640 , $height = 480)
    ];
});
