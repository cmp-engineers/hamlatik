<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Admin\Advertise::class, function (Faker $faker) {

    return [
        //
        'name' => $faker->name,
        'mobile' => $faker->phoneNumber,
        'image' =>  $faker->image( 'public/storage/images',  250, 250,null,false),
        'snapchat' => $faker->url,
        'instegram' => $faker->url,
        'twitter' => $faker->url,
        'facebook' => $faker->url,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});
