<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Admin\Offer::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'image' => $faker->image('public/storage/images',640 ,640,null,null),
        'vendor_id' => $faker->numberBetween($min = 1, $max = 25)

    ];
});
