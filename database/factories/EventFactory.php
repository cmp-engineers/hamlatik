<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Admin\Event::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'arabic_date' => $faker->date('Y-m-d'),
    ];
});
