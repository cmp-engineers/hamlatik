<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Admin\PrayerName::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
    ];
});
