<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Admin\Prayer::class, function (Faker $faker) {
    return [
        'prayer_names_id' => $faker->numberBetween($max = 1 , $max = 20),
        'prayer_categories_id' => $faker->numberBetween($min =1 , $max = 20),
        'prayer_sub_categories_id' => $faker->numberBetween($min = 1, $max = 20),
        'prayertext' => $faker->text($maxNbChars = 200)
    ];
});
