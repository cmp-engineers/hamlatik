<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Admin\Place::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'category_id' => $faker->numberBetween($min = 1 , $max = 4),
        'city_id' => $faker->numberBetween($min = 1 , $max = 10),
        'image' => $faker->image('public/storage/images' , 250, 250, null, null),
        'address' => $faker->address,
        'mobile' => $faker->phoneNumber,
    ];
});
