<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Admin\PrayerSubCategory::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'prayer_categories_id' => $faker->numberBetween($min = 1 , $max = 20),
    ];
});
